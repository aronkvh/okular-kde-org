# okular.kde.org

<!--- This should be updated to point to the correct image one the MR is merged --->
![An screenshot of okular's homepage](https://invent.kde.org/carlschwan/okular-kde-org/-/raw/hugo/static/images/screenies/Home.png)

-----

This is the git repository for okular.kde.org, the website for Okular, the universal document reader developed by kde. The site itself is built using [HUGO Framework](https://gohugo.io/), which uses go to generate static websites. If you want to test the website by yourself:

### Prerequisites

You will need to have the following packages installed in your system:

* HUGO: The framework itself we are using for deploying this website
* Go: The [aether-sass](https://invent.kde.org/websites/aether-sass) theme, which this website needs, is included as a Go submodule. You must manually install go in your system before trying to build the website; if go is not availaible, the build process will not work as expected
* Git: This is the file versioning system we use to store the files

### Build the Site

1. Clone this repository: `git clone https://invent.kde.org/websites/okular-kde-org.git`
2. Step into the directory: `cd okular-kde-org`
    * To see a live version of the site that updates as you modify the source code, use `hugo server`
    * To get the final HTML, ready to be deployed, use `hugo` and check the `./public` folder

### Build the translations

See [extractor docmentation](https://invent.kde.org/websites/hugo-i18n-extractor)
