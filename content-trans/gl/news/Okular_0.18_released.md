---
date: 2013-12-12
title: Publicouse Okular 0.18
---
A versión 0.18 de Okular publicouse xunto coa versión 4.12 das aplicacións de KDE. Esta versión introduce novas funcionalidades como a compatibilidade con son e vídeo en ficheiros EPub e tamén melloras en funcionalidades existentes como busca e impresión. Okular 0.18 é unha actualización recomendada para calquera que use Okular.

