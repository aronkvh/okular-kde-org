---
date: 2008-04-02
title: Publicouse Okular 0.6.3
---
A terceira versión de mantemento da serie 4.0 de KDE inclúe Okular 0.6.3. Inclúe algunhas correccións de fallos, por exemplo un mellor xeito de obter a posición do texto nun documento PDF, e un par de correccións para o sistema de anotacións e o índice. Pode consultar todos os problemas solucionados en <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>

