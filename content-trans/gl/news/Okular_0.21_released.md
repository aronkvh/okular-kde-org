---
date: 2014-12-17
title: Publicouse Okular 0.21
---
A versión 0.21 de Okular publicouse xunto coa versión 14.12 das aplicacións de KDE. Esta versión introduce novas funcionalidades como a busca inversa de latex-synctex en dvi e pequenas solucións de problemas. Okular 0.21 é unha actualización recomendada para calquera que use Okular.

