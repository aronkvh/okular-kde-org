---
date: 2012-10-10
title: Okular-Forum
---
Es gibt jetzt ein Unterforum innerhalb der KDE-Community-Foren. Sie finden es auf der Seite <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</href>.

