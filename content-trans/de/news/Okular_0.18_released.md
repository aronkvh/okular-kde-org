---
date: 2013-12-12
title: Okular 0.18 veröffentlicht
---
Die Version 0.18 von Okular wurde zusammen mit den KDE-Programmen 4.12 veröffentlicht. Diese Veröffentlichung enthält neue Funktionen wie Unterstützung für Audio und Video in EPUB-Dateien und auch Verbesserungen vorhandener Funktionen wie Suchen und Drucken. Okular 0.18 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.

