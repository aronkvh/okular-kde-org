---
date: 2009-06-03
title: Okular 0.8.4 veröffentlicht
---
Die vierte Wartungsversion der Reihe KDE 4.2 enthält Okular 0.8.4. Diese Version bringt Korrekturen für OpenDocument-Text-Dokumente und behebt Fehler, die Abstürze verhindern und ein paar Fehler in der Benutzeroberfläche. Eine Liste aller behobener Probleme finden Sie <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">hier</a>.

