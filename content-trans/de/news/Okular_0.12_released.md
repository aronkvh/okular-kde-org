---
date: 2011-01-26
title: Okular 0.12 veröffentlicht
---
Die Version 0.12 von Okular wurde zusammen mit den KDE-Programmen 4.6 veröffentlicht. Diese Veröffentlichung enthält kleine Korrekturen und Funktionen und ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.

