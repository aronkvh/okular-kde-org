---
date: 2019-08-15
title: Okular 1.8 veröffentlicht
---
Die Version 1.8 von Okular wurde zusammen mit den KDE-Anwendungen 19.08 veröffentlicht. Diese Veröffentlichung enthält die neue Funktion Linienenden für die Linien-Anmerkung sowie Fehlerkorrekturen und Verbesserungen. Das vollständige Änderungsprotokoll finden Sie auf der Seite <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=19.08.0#okular</a>. Okular 1.8 ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.

