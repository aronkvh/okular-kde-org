---
date: 2013-02-06
title: Okular 0.16 veröffentlicht
---
Die Version 0.16 von Okular wurde zusammen mit den KDE-Programmen 4.10 veröffentlicht. Diese Veröffentlichung enthält neue Funktionen wie die Fähigkeit, höhere Vergrößerungen anzuzeigen, ein Betrachter für Ihr Tablett, bessere Unterstützung für PDF-Filme, die Ansicht folgt der Auswahl und Verbesserungen bei der Bearbeitung von Anmerkungen. Diese Version ist eine empfohlene Aktualisierung für jeden, der Okular benutzt.

