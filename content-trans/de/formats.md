---
intro: Okular supports a wide variety of document formats and use cases. This page
  always refers to the stable series of Okular, currently Okular 20.12
layout: formats
menu:
  main:
    name: Dokumentformat
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Status der Unterstützung für Dokumentformate
---


