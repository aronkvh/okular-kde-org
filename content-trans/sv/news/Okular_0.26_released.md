---
date: 2016-08-18
title: Okular 0.26 utgiven
---
Version 0.26 av Okular har givits ut tillsammans med utgåva 16.08 av KDE:s program. Utgåvan introducerar mycket små ändringar. Den fullständiga ändringsloggen finns på <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. Okular 0.26 är en rekommenderad uppdatering för alla som använder Okular.

