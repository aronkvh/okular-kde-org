---
date: 2009-03-04
title: Okular 0.8.1 utgiven
---
Den första underhållsutgåvan i KDE 4.2-serien inkluderar Okular 0.8.1. Den innehåller några kraschfixar i CHM- och DjVu-gränssnitten, och mindre rättningar av användargränssnittet. Man kan läsa alla rättade problem på <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>

