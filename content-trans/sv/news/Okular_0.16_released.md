---
date: 2013-02-06
title: Okular 0.16 utgiven
---
Version 0.16 av Okular har givits ut tillsammans med utgåva 4.10 av KDE:s program. Utgåvan introducerar nya funktioner, som möjlighet att zooma till större förstoring i PDF-dokument, en Aktiv-baserad visare för läsplattor, mer stöd för PDF-filmer, visning följer markering, och förbättringar av kommentarredigering. Okular 0.16 är en rekommenderad uppdatering för alla som använder Okular.

