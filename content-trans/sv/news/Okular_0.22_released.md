---
date: 2015-04-15
title: Okular 0.22 utgiven
---
Version 0.22 av Okular har givits ut tillsammans med utgåva 15.04 av KDE:s program. Utgåvan är en rekommenderad uppdatering för alla som använder Okular.

