---
date: 2014-12-17
title: Okular 0.21 utgiven
---
Version 0.21 av Okular har givits ut tillsammans med utgåva 14.12 av KDE:s program. Utgåvan introducerar nya funktioner som latex-synctex omvänd sökning i DVI och mindre felrättningar. Okular 0.21 är en rekommenderad uppdatering för alla som använder Okular.

