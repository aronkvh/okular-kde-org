---
date: 2009-04-02
title: Okular 0.8.2 utgiven
---
Den andra underhållsutgåvan i KDE 4.2-serien inkluderar Okular 0.8.2. Den innehåller bättre stöd för omvänd sökning med DVI och pdfsync (som förhoppningsvis fungerar), samt rättningar och små förbättringar av presentationsläget. Man kan läsa alla rättade problem på <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>

