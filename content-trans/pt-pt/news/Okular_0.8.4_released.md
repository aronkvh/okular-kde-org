---
date: 2009-06-03
title: O Okular 0.8.4 foi lançado
---
A quarta versão de manutenção da série KDE 4.2 inclui o Okular 0.8.4. Inclui algumas correcções nos documentos de OpenDocument Text, algumas correcções de estoiros e alguns pequenos erros na interface. Poderá ver todas as questões corrigidas em <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php</a>

