---
date: 2012-08-01
title: O Okular 0.15 foi lançado
---
A versão 0.15 do Okular foi lançada em conjunto com a versão 4.9 das aplicações do KDE. Esta versão introduz funcionalidades novas, como a capacidade de gravar as anotações com os documentos PDF, mais suporte para os filmes em PDF, vários favoritos por página e mais algumas pequenas melhorias. Poderá encontrar uma lista incompleta dos erros e funcionalidades no <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;product=okular&amp;long_desc_type=allwordssubstr&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=exact&amp;email1=&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2010-10-10&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.9.0&amp;cmdtype=doit&amp;order=Reuse+same+sort+as+last+time&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=">Bugzilla</a>. O Okular 0.15 é uma actualização recomendada para todos os que utilizam o Okular.

