---
date: 2010-08-10
title: O Okular 0.11 foi lançado
---
A versão 0.11 do Okular foi lançada em conjunto com a versão 4.5 das Aplicações do KDE. Esta versão introduz algumas pequenas correcções e funcionalidades, e é uma actualização recomendada para todos os que usam o Okular.

