---
date: 2009-01-27
title: O Okular 0.8 foi lançado
---
A equipa do Okular orgulha-se de anunciar uma nova versão do Okular, lançada como parte do <a href="http://www.kde.org/announcements/4.2/">KDE 4.2</a>. Algumas das novas funcionalidade e melhorias incluem (por ordem aleatória): <ul type="disc"> <li>Adicionar a possibilidade de criar um Arquivo de Documento</li> <li>Iniciar uma pesquisa desde a página actual</li> <li>Suporte preliminar para vídeos nos documentos PDF</li> <li>Melhorias no modo de apresentação: inibição do protector do ecrã e modo de ecrã negro</li> <li>Mais anotações por carimbos</li> <li>Suporte para as ligações "apontar-e-carregar" do Lilypond</li> <li>Configuração do editor para a pesquisa inversa</li> <li>Exportação para OpenDocument Text e HTML nas infra-estruturas de ODT, Fictionbook e EPub (necessita do Qt 4.5)</li> <li>Nova infra-estrutura de documentos de fax G3/G4</li> <li>Infra-estrutura de DjVu: melhorias na gestão de memória</li> <li>Infra-estrutura de OpenDocument Text: diversas melhorias</li> <li>... outras pequenas funcionalidades e correcções</li> </ul> E, obviamente, foram corrigidos muitos erros.

