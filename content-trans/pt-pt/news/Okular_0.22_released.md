---
date: 2015-04-15
title: O Okular 0.22 foi lançado
---
A versão 0.22 do Okular foi lançada em conjunto com a versão 15.04 das Aplicações do KDE. Esta versão é uma actualização recomendada para todos os que usam o Okular.

