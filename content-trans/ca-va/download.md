---
intro: L'Okular està disponible com a paquet precompilat en una gran varietat de plataformes.
  Podeu comprovar l'estat del paquet per a la vostra distribució de Linux de la dreta,
  o continueu llegint per a informació en altres sistemes operatius
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: L'Okular ja està disponible a la majoria de les distribucions de Linux. El
    podeu instal·lar des del [Centre de programari de KDE](https://apps.kde.org/okular).
- image: /images/flatpak.png
  image_alt: Logotip del Flatpak
  name: Flatpak
  text: Podeu instal·lar el darrer [Flatpak de l'Okular](https://flathub.org/apps/details/org.kde.okular)
    des de Flathub. Els Flatpaks experimentals amb les construccions nocturnes de
    l'Okular es poden [instal·lar des del repositori Flatpak de KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications
    ).
- image: /images/ark.svg
  image_alt: Logotip de l'Ark
  name: Codi font del llançament
  text: L'Okular es publica amb regularitat com a part de les aplicacions KDE. Si
    el voleu construir a partir del codi font, podeu revisar la [secció Construcció](/build-it).
- image: /images/windows.svg
  image_alt: Logotip del Windows
  name: Windows
  text: Doneu una ullada a la [iniciativa de KDE al Windows](https://community.kde.org/Windows)
    per a informació sobre com instal·lar el programari KDE al Windows. La publicació
    estable està disponible a la [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8).
    També hi ha [compilacions nocturnes experimentals](https://binary-factory.kde.org/job/Okular_Nightly_win64/),
    per a les quals s'agraeixen les proves i els informes d'errors.
sassFiles:
- /sass/download.scss
title: Descarrega
---


