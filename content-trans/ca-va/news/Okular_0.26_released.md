---
date: 2016-08-18
title: Publicat l'Okular 0.26
---
S'ha publicat la versió 0.26 de l'Okular conjuntament amb la publicació 16.08 de les Aplicacions del KDE. Aquesta publicació presenta canvis molt petits. Podeu revisar el registre complet de canvis <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. L'Okular 0.26 és una actualització recomanada per a tothom que usi l'Okular.

