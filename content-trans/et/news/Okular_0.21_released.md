---
date: 2014-12-17
title: Ilmus Okular 0.21
---
Okulari versioon 0.21 ilmus koos KDE rakenduste väljalaskega 14.12. See väljalase sisaldab uusi võimalusi, näiteks latex-synctex pöördotsing DVI-failides, ja väiksemaid parandusi. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 0.21 peale.

