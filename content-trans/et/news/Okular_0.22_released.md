---
date: 2015-04-15
title: Ilmus Okular 0.22
---
Okulari versioon 0.22 ilmus koos KDE rakenduste väljalaskega 15.04. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 0.22 peale.

