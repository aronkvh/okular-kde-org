---
date: 2018-04-19
title: Ilmus Okular 1.4
---
Okulari versioon 1.4 ilmus koos KDE rakenduste väljalaskega 18.04. See väljalase sisaldab PDF-failide JavaScripti toetuse täiustusi ning toetab PDF-i renderdamise katkestamist, mis tähendab, et kui sul on keerukas PDF-fail ja sa muudad renderdamise ajal suurendust, katkestatakse see otsekohe, mitte ei oodata renderdamise lõppemist. Täielikku muutuste logi näeb aadressil <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 1.4 peale.

