---
date: 2014-04-16
title: Ilmus Okular 0.19
---
Okulari versioon 0.19 ilmus koos KDE rakenduste väljalaskega 4.13. See väljalase sisaldab uusi võimalusi, näiteks kaartide toetus kasutajaliideses, DPI ekraani kasutamine, et lehekülje suurus vastaks tegelikule paberisuurusele, parandused tagasivõtmise/uuestitegemise raamistikus, ning muid parandusi ja täiustusi. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 0.19 peale.

