---
date: 2016-12-15
title: Ilmus Okular 1.0
---
Okulari versioon 1.0 ilmus koos KDE rakenduste väljalaskega 16.12. See väljalase põhineb nüüd KDE Frameworks 5-l. Täielikku muutuste logi näeb aadressil <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 1.0 peale.

