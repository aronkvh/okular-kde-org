---
date: 2018-08-16
title: Ilmus Okular 1.5
---
Okulari versioon 1.5 ilmus koos KDE rakenduste väljalaskega 18.08. See väljalase sisaldab väiksemate paranduste ja uute võimaluste kõrval vormide kasutamise täiustusi. Täielikku muutuste logi näeb aadressil <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=1808.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.08.0#okular</a>. Kõigil, kes kasutavad Okulari, on soovitatav uuendada versiooni 1.5 peale.

