---
date: 2008-11-04
title: Ilmus Okular 0.7.3
---
KDE 4.1 seeria kolmas hooldusväljalase sisaldab Okulari 0.7.3. See sisaldab mõningaid väiksemaid parandusi kasutajaliides ja tekstiotsingus. Kõiki parandatud vigu näeb muutuste logis <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>

