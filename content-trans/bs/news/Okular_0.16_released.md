---
date: 2013-02-06
title: Okular 0.16 pušten
---
Verzija 0.16 Okulara je puštena zajedno sa KDE 4.10 aplikacijom. Ovo izdanje predstavlja nove mogućnosti kao što su uvećavanje do  viših nivoa u PDF dokumentima, aktivni preglednik za vaš tablet, više podrške za PDF filmove, okvir koji slijedi izbor te poboljšano uređivanje bilješki.  Okular 0.16 je preporučena nadogradnja za sve korisnike Okulara.

