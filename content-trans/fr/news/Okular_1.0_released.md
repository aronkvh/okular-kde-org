---
date: 2016-12-15
title: Publication de la version 1.0 d'Okular
---
La version 1.0 d'Okular a été livrée avec les applications fournies avec la version 16.12 de KDE Applications. Cette version repose sur la version 5 du framework KDE. Vous pouvez lire la liste des changements à cette adresse : <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. La version 1.0 d'Okular est une mise à jour recommandée pour toute personne utilisant Okular. 

