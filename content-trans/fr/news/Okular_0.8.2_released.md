---
date: 2009-04-02
title: Publication de la version 0.8.2 d'Okular
---
La version 0.8.2 d'Okular a été livrée avec la deuxième version de maintenance de KDE 4.2. Elle fournit une meilleure prise en charge (fonctionnant heureusement fort bien) des recherches en arrière dans les fichiers « DVI » et « pdf-sync », ainsi que des corrections et des améliorations mineures dans le mode de présentation. Vous pouvez prendre connaissance de tous les problèmes corrigés à l'adresse <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>

