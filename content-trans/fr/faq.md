---
faq:
- answer: Ubuntu (thus Kubuntu as well) packages of Okular are compiled without support
    for these two formats. The reason is explained in [this](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    Launchpad report.
  question: When using Ubuntu, I cannot read CHM and EPub documents, even if I have
    okular-extra-backends and libchm1 installed. Why?
- answer: Because you don't have a speech service on your system. Install the Qt Speech
    library and they should be enabled
  question: Why are the speak options in the Tools menu grayed out?
- answer: Veuillez installer le paquet « poppler-data »
  question: Certains caractères ne sont pas rendus et quand vous activez le mode de
    débogage, certaines lignes mentionnent « Paquet de langues manquants pour xxx »
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Questions fréquemment posées
---


