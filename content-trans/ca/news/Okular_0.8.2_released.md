---
date: 2009-04-02
title: Publicat l'Okular 0.8.2
---
La segona publicació de manteniment de les sèries 4.2 del KDE conté l'Okular 0.8.2. Inclou un millor suport (esperem que funcioni) per a la cerca inversa dels DVI i pdfsync, i esmenes i petites millores al mode presentació. Podeu llegir tots els problemes solucionats a <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>

