---
date: 2018-08-16
title: Publicat l'Okular 1.5
---
S'ha publicat la versió 1.5 de l'Okular conjuntament amb la publicació 18.08 de les Aplicacions del KDE. Aquesta publicació presenta millores als formularis entre altres esmenes i petites característiques. Podeu revisar el registre complet de canvis <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. L'Okular 1.5 és una actualització recomanada per a tothom que usi l'Okular.

