---
date: 2013-12-12
title: Publicat l'Okular 0.18
---
S'ha publicat la versió 0.18 de l'Okular conjuntament amb la publicació 4.12 de les Aplicacions del KDE. Aquesta publicació presenta característiques noves com la implementació d'àudio/vídeo als fitxers EPub, i també millores a les característiques existents com la cerca i la impressió. L'Okular 0.18 és una actualització recomanada per a tothom que usi l'Okular.

