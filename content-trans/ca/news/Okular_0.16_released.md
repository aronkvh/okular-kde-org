---
date: 2013-02-06
title: Publicat l'Okular 0.16
---
S'ha publicat la versió 0.16 de l'Okular conjuntament amb la publicació 4.10 de les Aplicacions del KDE. Aquesta publicació presenta característiques noves com la capacitat de fer zoom a un nivell més gran als documents PDF, un visor basat en Active per als dispositius de tauleta, més suport per a les pel·lícules PDF, l'àrea de visualització segueix la selecció, i millores a l'edició d'anotacions. L'Okular 0.16 és una actualització recomanada per a tothom que usi l'Okular.

