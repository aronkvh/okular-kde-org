---
date: 2008-03-05
title: O Okular 0.6.2 foi lançado
---
A segunda versão de manutenção da série KDE 4.0 inclui o Okular 0.6.2. Ela incorpora algumas correções de erros, incluindo uma melhor estabilidade ao fechar um documento, assim como pequenas correções do sistema de favoritos. Você pode ler todos os detalhes das correções em <a href="http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_1to4_0_2.php</a>

