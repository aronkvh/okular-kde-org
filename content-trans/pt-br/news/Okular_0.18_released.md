---
date: 2013-12-12
title: O Okular 0.18 foi lançado
---
A versão 0.18 do Okular foi lançada junto com a versão 4.12 do KDE Applications. Esta versão introduz novas funcionalidades, como suporte a áudio/vídeo em arquivos EPub e também melhorias nas funcionalidades já existentes, como pesquisa e impressão. É uma atualização recomendada a todos os usuários do Okular.

