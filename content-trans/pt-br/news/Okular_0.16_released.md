---
date: 2013-02-06
title: O Okular 0.16 foi lançado
---
A versão 0.16 do Okular foi lançada junto com a versão 4.10 do KDE Applications. Esta versão introduz novas funcionalidades, como a habilidade maiores ampliações de documentos em PDF; um visualizador para tablets baseado no Active; melhor suporte para vídeos em arquivos PDF; a janela de exibição segue a seleção; e melhorias na edição de anotações. É uma atualização recomendada a todos os usuários do Okular.

