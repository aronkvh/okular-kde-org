---
date: 2014-04-16
title: O Okular 0.19 foi lançado
---
A versão 0.19 do Okular foi lançada junto com a versão 4.13 do KDE Applications. Esta versão introduz novas funcionalidades, como suporte a abas na interface, uso de resolução de tela que faz com que a página corresponda ao tamanho real do papel, melhorias nas funcionalidades de Desfazer/Refazer e outros refinamentos. É uma atualização recomendada a todos os usuários do Okular.

