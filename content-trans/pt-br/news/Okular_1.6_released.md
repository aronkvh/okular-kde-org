---
date: 2018-12-13
title: O Okular 1.6 foi lançado
---
A versão 1.6 do Okular foi lançada junto com o KDE Applications versão 18.12. Esta versão introduz a nova ferramenta de anotação máquina de escrever entre várias outras correções e melhorias. Você pode verificar o registro de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=18.12.0#okular</a>. O Okular 1.6 é uma atualização recomendada a todos os usuários do Okular.

