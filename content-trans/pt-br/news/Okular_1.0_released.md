---
date: 2016-12-15
title: O Okular 1.0 foi lançado
---
A versão 1.0 do Okular foi lançada junto com o KDE Applications versão 16.12. Esta versão é agora baseada no KDE Frameworks 5, você pode verificar o registro de alterações completo em <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. O Okular 1.0 é uma atualização recomendada a todos os usuários do Okular.

