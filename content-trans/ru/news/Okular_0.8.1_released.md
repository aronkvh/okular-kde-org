---
date: 2009-03-04
title: Выпущен Okular 0.8.1
---
Первый выпуск KDE 4.2 включает в себя Okular 0.8.1. В выпуске Okular 0.8.1 были исправлены некоторые ошибки, приводящие к аварийному завершению программы при работе с файлами CHM и DjVu, и небольшие изменения в пользовательском интерфейсе. Вы можете прочитать обо всех исправлениях на <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a>

