---
date: 2009-04-02
title: Выпущен Okular 0.8.2
---
Второй выпуск KDE 4.2 включает в себя Okular 0.8.2. Он включает лучшую поддержку обратного поиска в DVI и PDF и улучшения в работе режима презентации. Вы можете прочитать обо всех исправлениях на <a href="http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_1to4_2_2.php</a>

