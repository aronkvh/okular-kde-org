---
date: 2017-08-17
title: Okular 1.2 wydany
---
Wersja 1.2 Okulara została wydana wraz z wydaniem Aplikacji do KDE 17.08. To wydanie wprowadza mniejsze poprawki błędów i usprawnienia. Pełny dziennik zmian możesz obejrzeć na  <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.08.0#okular</a>. Zaleca się uaktualnienie do Okulara 1.2 wszystkim użytkownikom.

