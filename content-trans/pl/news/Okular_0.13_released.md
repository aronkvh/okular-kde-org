---
date: 2011-07-27
title: Okular 0.13 wydany
---
Wersja 0.13 programu Okular została opublikowana wraz z Aplikacjami KDE w wersji 4.7. To wydanie wprowadza drobne poprawki oraz funkcje i jest zalecaną aktualizacją dla wszystkich użytkowników Okular.

