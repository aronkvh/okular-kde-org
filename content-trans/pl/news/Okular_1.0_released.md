---
date: 2016-12-15
title: Okular 1.0 wydany
---
Wersja 1.0 Okulara została wydana wraz z wydaniem Aplikacji do KDE 16.12. To wydanie jest od teraz oparte na Szkieletach KDE 5, pełny dziennik zmian możesz obejrzeć na  <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.12.0#okular</a>. Zaleca się uaktualnienie do Okulara 1.0 wszystkim użytkownikom.

