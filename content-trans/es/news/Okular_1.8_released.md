---
date: 2019-08-15
title: Okular 1.8 publicado
---
La versión 1.8 de Okular ha sido publicada junto al lanzamiento de las Aplicaciones de KDE 19.08. Esta versión introduce la funcionalidad de definir los finales de las líneas de anotación entre otras correcciones y funcionalidades menores. Puede consultar el registro de cambios completo en <a href='https://www.kde.org/announcements/fulllog_applications- aether.php?version=19.08.0#okular'>https://www.kde.org/announcements/fulllog_applications-aether.php?version=19.08.0#okular</a>. Okular 1.8 es una actualización recomendada para todos los usuarios de Okular.

