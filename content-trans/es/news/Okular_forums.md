---
date: 2012-10-10
title: Foros de Okular
---
En la actualidad tenemos un foro dentro de los Foros de la Comunidad KDE. Puede acceder a él en <a href="http://forum.kde.org/viewforum.php?f=251">http://forum.kde.org/viewforum.php?f=251</a>.

