---
date: 2007-01-31
title: Okular se une al proyecto Temporada de Usabilidad
---
El equipo de Okular se enorgullece de anunciar que Okular ha sido una de las aplicaciones seleccionadas para participar en el proyecto <a href="http://www.openusability.org/season/0607/">Temporada de usabilidad</a>, dirigido por expertos en usabilidad de <a href="http://www.openusability.org">OpenUsability</a>. Desde aquí damos la bienvenida a Sharad Baliyan al equipo y agradecemos a Florian Graessle y Pino Toscano su trabajo constante para mejorar Okular.

