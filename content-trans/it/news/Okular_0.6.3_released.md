---
date: 2008-04-02
title: Rilasciato Okular 0.6.3
---
Il terzo rilascio di manutenzione della serie KDE 4.0 comprende Okular 0.6.3. Include alcune correzioni degli errori, per es. un modo migliore per avere la posizione del testo in un documento PDF e un paio di correzioni del sistema di annotazioni e del sommario. Tutti i problemi risolti sono consultabili in <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php</a>

