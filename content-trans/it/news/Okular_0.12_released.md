---
date: 2011-01-26
title: Rilasciato Okular 0.12
---
La versione 0.12 di Okular è stato rilasciata insieme con le applicazioni di KDE 4.6. Questo rilascio introduce piccole correzioni e funzionalità ed è un aggiornamento consigliato per chiunque utilizzi Okular.

