---
date: 2011-07-27
title: Rilasciato Okular 0.13
---
La versione 0.13 di Okular è stato rilasciata insieme con le applicazioni di KDE 4.7. Questo rilascio introduce piccole correzioni e funzionalità ed è un aggiornamento consigliato per chiunque utilizzi Okular.

