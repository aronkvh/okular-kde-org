---
date: 2020-04-23
title: Rilasciato Okular 1.10
---
È stata rilasciata la versione 1.10 di Okular. Questo rilascio introduce lo scorrimento cinetico, miglioramenti nella gestione delle schede e nell'interfaccia grafica, oltre a varie correzioni minori e aggiunte di funzionalità dappertutto. Puoi verificare le novità di versione all'indirizzo <a href='https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.04.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.

