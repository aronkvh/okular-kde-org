---
date: 2008-11-04
title: Rilasciato Okular 0.7.3
---
Il terzo rilascio di manutenzione della serie KDE 4.1 comprende Okular 0.7.3. Include alcune correzioni minori dell'interfaccia utente e nella ricerca testuale. Tutti i problemi risolti sono consultabili in <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>

