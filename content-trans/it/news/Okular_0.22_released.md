---
date: 2015-04-15
title: Rilasciato Okular 0.22
---
La versione 0.22 di Okular è stato rilasciata insieme con le applicazioni di KDE 15.04. Okular 0.22 è un aggiornamento consigliato per chiunque utilizzi Okular.

