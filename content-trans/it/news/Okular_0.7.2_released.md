---
date: 2008-10-03
title: Rilasciato Okular 0.7.2
---
Il secondo rilascio di manutenzione della serie KDE 4.1 comprende Okular 0.7.2. Include alcune correzioni minori nei motori TIFF e Comikbook e il passaggio a «Adatta larghezza» come livello predefinito di zoom. Tutti i problemi risolti sono consultabili in <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>

