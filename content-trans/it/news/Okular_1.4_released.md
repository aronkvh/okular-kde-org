---
date: 2018-04-19
title: Rilasciato Okular 1.4
---
La versione 1.4 di Okular è stato rilasciata insieme con le applicazioni di KDE 18.04. Questo rilascio introduce il supporto migliorato per JavaScript nei file PDF e supporta l'annullamento della resa PDF, il che significa che se hai un file PDF complesso e cambi l'ingrandimento durante l'elaborazione della resa, questa sarà annullata immediatamente anziché attendere la fine dell'elaborazione. Puoi verificare le novità di versione all'indirizzo <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. È un aggiornamento consigliato per chiunque utilizzi Okular.

