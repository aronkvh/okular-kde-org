---
date: 2008-10-03
title: Okular 0.7.2 julkaistiin
---
Okular 0.7.2 on julkaistu KDE 4.1-sarjan toisessa korjausjulkaisussa. Tähän julkaisuversioon kuuluu pieniä korjauksia TIFF- ja Comicbook-taustaosiin. Oletuszoomaustasoksi on myös vaihdettu ”Sovita leveyteen”. Voit lukea kaikista korjatuista ongelmista osoitteessa <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php</a>

