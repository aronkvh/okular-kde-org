---
date: 2009-01-27
title: Okular 0.8 julkaistiin
---
Okularin kehitysryhmä ylpeänä ilmoittaa uudesta Okularin versiosta, joka on julkaistu osana <a href="http://www.kde.org/announcements/4.2/">KDE 4.2:ta</a>. Julkaisuversioon kuuluu mm. seuraavat uudet ominaisuudet ja parannukset (satunnaisessa järjestyksessä): <ul type="disc"> <li>asiakirjan arkistointimahdollisuus</li> <li>haun aloittaminen nykyiseltä sivulta</li> <li>alustava tuki PDF-asiakirjoissa oleville videoille</li> <li>parannuksia esitystilaan: näytönsäästäjän estäminen ja mustan näytön tila</li> <li>lisää leimamerkintöjä</li> <li>tuki Lilypondin osoita ja napsauta -linkeille</li> <li>muokkainasetukset käännöshaulle</li> <li>vienti HTML- ja OpenDocument Text -muotoihin ODT-, Fictionbook- ja EPub-taustaosien osalta (vaatii Qt 4.5:n)</li> <li>uusi taustaosa G3/G4-faksiasiakirjoille</li> <li>DjVu-taustaosa: parannuksia muistinhallintaan</li> <li>OpenDocument Text -taustaosa: useita parannuksia</li> <li>muita pieniä ominaisuuksia ja korjauksia.</li> </ul>Lisäksi tietysti monia virheitä korjattiin.

