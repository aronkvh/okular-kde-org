---
date: 2014-12-17
title: Okular 0.21 julkaistiin
---
Okularin versio 0.21 on julkaistu KDE Applications 14.12:n mukana. Tässä julkaisuversiossa on uusia ominaisuuksia kuten LaTeX-SyncTeX-käänteishaun DVI-tiedostoissa ja pieniä korjauksia. Okular 0.21 on suositeltu päivitys kaikille Okularin käyttäjille.

