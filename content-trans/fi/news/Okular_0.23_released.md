---
date: 2015-08-19
title: Okular 0.23 julkaistiin
---
Okularin versio 0.23 on julkaistu KDE Applications 15.08:n mukana. Tässä julkaisuversiossa on uutta häivytyssiirtymä esitystilassa sekä joitakin korjauksia merkintätukeen ja videoiden toistoon. Okular 0.23 on suositeltu päivitys kaikille Okularin käyttäjille.

