---
date: 2010-08-10
title: Izšel je Okular 0.11
---
Različica 0.11 programa Okular je bila izdana skupaj z izdajo programov KDE 4.5. Ta izdaja predstavlja majhne popravke in funkcije in je priporočena posodobitev za vse, ki uporabljajo Okular.

