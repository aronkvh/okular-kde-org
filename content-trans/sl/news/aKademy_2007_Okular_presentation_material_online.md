---
date: 2007-07-10
title: aKademy 2007 predstavitev Okularja na spletu
---
Prispevek Pino Toscano o programu Okular na<a href="http://akademy2007.kde.org">aKademy 2007</a> je zdaj na spletu. Na voljo so tako <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">prosojnice</a> kot <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">video</a>.

