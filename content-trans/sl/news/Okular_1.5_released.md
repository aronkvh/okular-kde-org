---
date: 2018-08-16
title: Izšel je Okular 1.5
---
Različica 1.5 Okular je bila izdana skupaj s KDE Applications 18.08. Ta izdaja predstavlja izboljšave obrazca med različnimi popravki in manjšimi dodatnimi funkcijami. Celoten dnevnik sprememb lahko preverite na<a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.08.0#okular</a>. Okular 1.5 je priporočljiva posodobitev za vse, ki uporabljajo Okular.

