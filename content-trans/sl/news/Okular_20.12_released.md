---
date: 2020-12-10
title: Izšel je Okular 20.12
---
Izšla je različica Okularja 20.12. V tej izdaji so predstavljeni različni manjši popravki in funkcije. Celoten dnevnik sprememb lahko preverite na <a href='https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular'>https://kde.org/announcements/changelog-releases.php?version=20.12.0#okular</a>. Okular 20.12 je priporočljiva posodobitev za vse, ki uporabljajo Okular.

