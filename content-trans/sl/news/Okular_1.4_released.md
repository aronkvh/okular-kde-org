---
date: 2018-04-19
title: Izšel je Okular 1.4
---
Različica 1.4 Okular je bila izdana skupaj s KDE Applications 18.04. Ta izdaja uvaja izboljšano podporo za JavaScript v datotekah PDF in podpira preklic upodabljanja PDF, kar pomeni, da če imate zapleteno datoteko PDF in spremenite povečavo med upodabljanjem, se bo takoj preklicala, namesto da bi čakala, da se upodabljanje konča. Celoten dnevnik sprememb lahko preverite na <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a>. Okular 1.4 je priporočljiva posodobitev za vse, ki uporabljajo Okular.

