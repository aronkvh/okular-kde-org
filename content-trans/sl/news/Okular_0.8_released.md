---
date: 2009-01-27
title: Izšel je Okular 0.8
---
Skupina Okular s ponosom napoveduje novo različico Okularja, ki je izšla v okviru <a href="http://www.kde.org/announcements/4.2/">KDE 4.2</a>. Nekatere nove funkcije in izboljšave vključujejo (v naključnem vrstnem redu): <ul type="disc"><li>Dodano možnost arhiviranja dokumentov</li><li> Začetek iskanja s trenutne strani</li><li >Predhodna podpora za videoposnetke v dokumentih PDF</li><li>Izboljšave načina predstavitve: zapora ohranjevalnika zaslona in načina črnega zaslona</li><li>Več zaznamkov žigov</li><li>Podpora za Lilypondove povezav "pokaži in klikni"</li><li> Konfiguracija urejevalnika za inverzno iskanje</li><li>Izvozi v besedilo OpenDocument in HTML za zaledne datoteke ODT, Fictionbook in EPub (zahteva Qt 4.5)</li><li>Novo zaledje za faks dokumente G3/G4</li><li>Zaledje DjVu: izboljšave upravljanja pomnilnika</li><li>Zaledje besedila OpenDocument: različne izboljšave</li><li>...druge manjše funkcije in popravki</ li></ul>In seveda so bile odpravljene številne napake.

