---
date: 2008-11-04
title: Izšel je Okular 0.7.3
---
Tretja izdaja vzdrževanja serije KDE 4.1 vključuje Okular 0.7.3. Vključuje nekaj manjših popravkov v uporabniškem vmesniku in pri iskanju besedila. Opis vseh odpravljenih težav lahko preberete na <a href="http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php#okular">http://www.kde.org/announcements/changelogs/changelog4_1_2to4_1_3.php</a>

