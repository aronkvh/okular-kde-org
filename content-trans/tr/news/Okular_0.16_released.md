---
date: 2013-02-06
title: Okular 0.16 yayımlandı
---
Okular'ın 0.16 sürümü KDE uygulamalarının 4.10  sürümüyle birlikte yayımlandı. Bu sürüm PDF belgelerinde daha yüksek seviyelerde yakınlaştırma, tablet cihazlarınızda Active tabanlı görüntüleyici, PDF videoları için daha fazla destek, görünüm noktasının seçimi takip etmesi ve açıklama düzenleme ilerlemeleri. Okular 0.16, Okular kullanan herkes için önerilen bir güncellemedir.

