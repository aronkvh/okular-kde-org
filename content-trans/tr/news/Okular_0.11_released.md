---
date: 2010-08-10
title: Okular 0.11 yayımlandı
---
Okular'ın 0.11 sürümü KDE uygulamalarının 4.5 sürümüyle birlikte yayımlandı. Bu yayım küçük hata düzültmeleriyle özellikler içeriyor ve Okular kullanan herkese güncelleme yapmaları önerilir.

