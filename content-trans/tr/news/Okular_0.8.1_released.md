---
date: 2009-03-04
title: Okular 0.8.1 yayımlandı
---
KDE 4.2 serisinin ilk bakım sürümü Okular 0.8.1'i içeriyor. Bu sürüm CHM ve DjVu arka uçlarında bazı çökme düzeltmeleri ve kullanıcı arayüzünde küçük düzeltmeleri içeriyor. Düzeltilen tüm durumları  <a href="http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2to4_2_1.php</a> adresinde okuyabilirsiniz.

