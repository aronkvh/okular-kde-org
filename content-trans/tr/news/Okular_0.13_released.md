---
date: 2011-07-27
title: Okular 0.13 yayımlandı
---
Okular'ın 0.13 sürümü KDE uygulamalarının 4.7 sürümüyle birlikte yayımlandı. Bu yayım küçük hata düzültmeleriyle özellikler içeriyor ve Okular kullanan herkese güncelleme yapmaları önerilir.

