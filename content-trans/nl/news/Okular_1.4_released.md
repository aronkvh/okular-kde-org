---
date: 2018-04-19
title: Okular 1.4 vrijgegeven
---
De versie 1.4 van Okular is vrijgegeven samen met de vrijgave van KDE Applications 18.04. Deze vrijgave introduceert verbeterde ondersteuning van JavaScript in PDF bestanden en ondersteunt annuleren van PDF rendering, wat betekent dat als u een complex PDF-bestand hebt en u wijzigt de zoom terwijl het bezig is met renderen, het dat onmiddellijk zal annuleren, in plaatst van te wachten op het gereed zijn hiervan. U kunt de volledige log met wijzigingen bekijken op <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.04.0#okular</a>. Okular 1.4 wordt aanbevolen aan iedereen die Okular gebruikt.

