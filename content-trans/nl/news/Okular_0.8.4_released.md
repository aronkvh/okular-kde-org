---
date: 2009-06-03
title: Okular 0.8.4 vrijgegeven
---
De vierde onderhoudsuitgave van de KDE 4.2 serie bevat Okular 0.8.4. Het bevat reparaties in documenten in OpenDocument Text, een paar reparaties voor crashes en een paar kleine raparaties in het interface. U kunt over alle reparaties lezen op <a href="http://www.kde.org/announcements/changelogs/changelog4_2_3to4_2_4.php#okular">http://www.kde.org/announcements/changelogs/changelog4_2_3to4_3_4.php</a>

