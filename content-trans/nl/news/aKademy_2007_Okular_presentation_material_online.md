---
date: 2007-07-10
title: aKademy 2007 Okular presentatiemateriaal staat online
---
De lezing over Okular gegeven door Pino Toscano op <a href="http://akademy2007.kde.org">aKademy 2007</a> is nu online. Er zijn zowel <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">dia's</a> als <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">video</a> beschikbaar.

