---
date: 2013-12-12
title: Okular 0.18 vrijgegeven
---
De 0.18 versie van Okular is vrijgegeven samen met de vrijgave van KDE Applications 4.12. Deze vrijgave introduceert nieuwe functies zoals ondersteuning voor audio-/video- en EPub-bestanden en ook verbeteringen van bestaande functies zoals zoeken en afdrukken. Okular 0.18 wordt aanbevolen aan iedereen die Okular gebruikt.

