---
intro: Okular ondersteunt een brede variëteit van documentformaten en soorten van
  gebruik. Deze pagina refereert altijd naar de stabiele serie van Okular, op dit
  moment Okular 20.12
layout: formats
menu:
  main:
    name: Documentformaat
    parent: about
    weight: 1
sassFiles:
- /sass/table.scss
title: Status van documentformaatbehandelaars
---


