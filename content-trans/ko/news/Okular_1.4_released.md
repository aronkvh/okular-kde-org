---
date: 2018-04-19
title: Okular 1.4 출시
---
Okular 1.4 버전은 KDE 프로그램 18.04와 함께 출시되었습니다. 이 릴리스에서는 PDF 파일 내 자바스크립트 지원 개선, PDF 렌더링 취소 기능을 지원합니다. 복잡한 PDF 파일을 연 상태에서 크기를 조정했을 때 렌더링이 끝나는 것을 무한정 기다리지 않고 취소할 수 있습니다. 전체 변경 내역은 <a href='https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=18.04.0#okular</a> 페이지에서 확인할 수 있습니다. Okular 1.4는 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.

