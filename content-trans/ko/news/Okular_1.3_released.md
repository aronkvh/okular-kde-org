---
date: 2017-12-14
title: Okular 1.3 출시
---
Okular 1.3 버전은 KDE 프로그램 17.12와 함께 출시되었습니다. 이 릴리스에서는 주석 및 양식 데이터를 저장하는 방법을 변경하고, 렌더링 시간이 오래 걸리는 파일의 부분 렌더링 업데이트를 지원하고, 텍스트 선택 모드에서 텍스트 링크를 대화식으로 만들고, 파일 메뉴에 공유 옵션을 추가하고, Markdown 지원을 추가하고 HiDPI 지원과 관련된일부 문제를 수정했습니다. 전체 변경 내역은 <a href='https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=17.12.0#okular</a> 페이지에서 확인할 수 있습니다. Okular 1.3은 Okular를 사용하는 모든 사용자에게 권장되는 업데이트입니다.

