---
date: 2008-02-05
title: Okular 0.6.1 출시
---
KDE 4.0 시리즈의 첫 번째 유지 보수 릴리스에는 Okular 0.6.1이 들어 있습니다. 이 릴리스에는 저장할 때 파일 다운로드 방지, 사용성 개선 등 일부 버그가 수정되었습니다. 전체 변경 사항을 확인하려면 <a href="http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php#okular">http://www.kde.org/announcements/changelogs/changelog4_0to4_0_1.php</a> 페이지를 방문하십시오

