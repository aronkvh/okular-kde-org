---
faq:
- answer: Пакунки Okular для Ubuntu (а отже і Kubuntu) зібрано без підтримки цих двох
    форматів. Пояснення причини наведено у [цьому](https://bugs.launchpad.net/kdegraphics/+bug/277007)
    звіті щодо вади на Launchpad.
  question: В Ubuntu я не можу відкрити за допомогою програми документи CHM і EPub
    навіть після встановлення okular-extra-backends і libchm1. У чому причина?
- answer: Ви не встановили програм для синтезу мовлення з тексту у KDE. Встановіть
    бібліотеку Qt Speech і пункти стануть доступними.
  question: Чому пункти декламування тексту у меню «Інструменти» позначено сірим кольором?
- answer: Встановіть пакунок poppler-data
  question: Деякі символи не оброблялися. Після вмикання діагностики з’являлися згадки
    про те, що «Не встановлено мовного пакунка для xxx» («Missing language pack for
    xxx»).
layout: faq
menu:
  main:
    parent: about
    weight: 5
sassFiles:
- /sass/faq.scss
title: Поширені питання
---


