---
date: 2007-04-04
title: Okular на kdegraphics
---
Команда розробників Okular з радістю оголошує про те, що Okular тепер є частиною модуля kdegraphics. Наступну версію Okular буде випущено разом з KDE 4.0.

