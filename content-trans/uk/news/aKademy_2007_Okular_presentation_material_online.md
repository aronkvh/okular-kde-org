---
date: 2007-07-10
title: Інтернет-матеріали презентації Okular на aKademy 2007
---
Тепер ви можете ознайомитися у інтернеті з доповіддю Pino Toscano щодо Okular на <a href="http://akademy2007.kde.org">aKademy 2007</a>. Ви можете переглянути <a href="http://akademy2007.kde.org/conference/slides/okular.pdf">слайди</a> та <a href="http://home.kde.org/~akademy07/videos/2-05-Okular:simply_a_document_viewer.ogg">відео</a>.

