---
date: 2008-10-03
title: Випущено Okular 0.7.2
---
До складу другої модифікації випуску KDE 4.1 включено Okular 0.7.2. У новій версії виправлено вади у модулях TIFF і Comicbook та переведено програму на типове використання масштабування «Влаштувати ширину». Докладніше про виправлені вади можна дізнатися <a href="http://www.kde.org/announcements/changelogs/changelog4_1_1to4_1_2.php#okular">тут</a>

