---
menu:
  main:
    parent: about
    weight: 4
title: Kapcsolat
---
<img src="/images/konqi-dev.png" align="right" alt="Konqi, the KDE mascot" style="height: 200px;"/> 

Így tudod felvenni a kapcsolatot a fejlesztőkkel:
* Levelezőlisták: Az Okular fejlsztésének koordináláshoz az [okular-devel levelezőlistát](https://mail.kde.org/mailman/listinfo/okular-devel) használjuk, a kde.org-on. Használhatod  beszélgetésre az alkalmazás fejlesztéséről és értékeljük a visszajelzéseket a már meglévő vagy új backendekről.

* IRC: Általános beszélgetésre az IRC-t használjuk: [#okular](irc://irc.kde.org/#okular) és [#kde-devel](irc://irc.kde.org/#kde-devel) a[Freenode hálózaton](http://www.freenode.net/). Itt megtalálhatod az Okular fejlsztőit.

* Matrix: A fent említett chat elérhető a Matrix hálózatról is [#okular:kde.org](https://matrix.to/#/#okular:kde.org). 

* Fórum: Ha a fórumot preferálod használhatod az [Okular fórum](http://forum.kde.org/viewforum.php?f=251) fórumot a  [KDE Közössé Fórumai](http://forum.kde.org/)-n belül.

* Hibák és kérések: A hibabejelentéseket és a kéréseket a  [KDE hibakövető](https://bugs.kde.org/)-n várjuk. Ha szeretnél segíteni, [itt](https://community.kde.org/Okular) megtalálod a legtöbbször előforduló hibabejelentéseket.
