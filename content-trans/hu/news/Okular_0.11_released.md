---
date: 2010-08-10
title: Megjelent az Okular 0.11
---
Az Okular 0.11-es kiadása a KDE Applications 4.5-tel eggyütt jelent meg. Ebben a kiadásban kisebb javítások és új funkciók szerepelnek, a frissítés minden Okular felhasználónak ajánlott.

