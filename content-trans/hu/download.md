---
intro: Az Okular előre kész csomagként számos platformon elérhető.
  Ellenőrizheted Linux-disztribúciód csomagjának állapotát a jobb oldalon, vagy tovább olvashatsz a más operációs rendszerekre elérhető csomagokról.
layout: download
menu:
  main:
    parent: about
    weight: 3
options:
- image: /images/tux.png
  image_alt: Tux
  name: Linux
  text: Az Okular a legtöbb Linux-disztribúcióra elérhető. Telepítheted
   a [KDE Szoftverközpont](https://apps.kde.org/okular)-ból.
- image: /images/flatpak.png
  image_alt: Flatpak logo
  name: Flatpak
  text: Telepítheted a legújabb [Okular Flatpak-ot](https://flathub.org/apps/details/org.kde.okular)
    a Flathubról. Kísérleti Flatpak csomagok a legújabb Okular verzióval telepíthetőek a  [a KDE Flatkpak tárolóból]
    (https://community.kde.org/Guidelines_and_HOWTOs/Flatpak#Applications).
- image: /images/ark.svg
  image_alt: Ark logo
  name: Innét tölthető le:
  text: Az Okular rendszeresen megjelenik a KDE Alkalmazások részeként.
  Ha szeretnéd forráskódból összeállítani: [Build](/build-it).
- image: /images/windows.svg
  image_alt: Windows logo
  name: Windows
  text: Nézd meg a [KDE Windows-on kezdeményezést](https://community.kde.org/Windows), ha szeretnél kde szoftvereket telepíteni Windows  rendszerre. A stabil verzió elérhető a
    [Microsoft Store-ból](https://www.microsoft.com/store/apps/9n41msq1wnm8).
    Kisérlei verziók is elérhetők [itt](https://binary-factory.kde.org/job/Okular_Nightly_win64/).
    Megköszönjük, ha teszteled és jelented az észlelt hibákat.
sassFiles:
- /sass/download.scss
title: Letöltés
---


