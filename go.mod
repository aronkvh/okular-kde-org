module okular-kde-org

go 1.13

require (
	github.com/gohugoio/hugo-mod-bootstrap-scss-v4 v0.0.0-20200902213320-c9cb5e39d8c6 // indirect
	github.com/thednp/bootstrap.native v0.0.0-20210208065639-9692a29f3498 // indirect
	invent.kde.org/websites/aether-sass v0.0.0-20210403210106-44c9ce4c8090 // indirect
)
